# PAYFAZZ Main Repo

## How to run (Development)

**Do all of these on the project root directory**

* Clone this repository

* Update your bash-lib module
       
        git submodule init
        git submodule update
		
* Install all dependencies

    	./docker/node-8 exec npm install 
    	# or
    	npm install
    
	If the ```postinstall``` hook is not working, run lerna bootstrap manually
    
    	# this will do `lerna bootstrap --hoist` to symlink 
    	# all packages on lerna.json
    	./docker/node-8 exec npm run bootstrap
		
* Start all needed containers (postgresql, redis, node)
    
		# for node 8
    	./docker/node8-dev start
    
	    # or for node version 5, will be deprecated soon
    	./docker/node5-dev
    
* Execute commands using node container (for running the services, etc.)
    
		./docker/node8-dev exec
    
    **Example**:
    
    	# to run node cli
	    ./docker/node8-dev exec node
    
    	# to run payfazz services
	    ./docker/node8-dev exec gulp publicapi-run
	    ./docker/node8-dev exec gulp pas-run
    
	    # to run payfazz tests
	    ./docker/node8-dev exec gulp payment-test
	    ./docker/node8-dev exec gulp recharge-test
    
    	# etc.

## How to publish (Production)

- Run the publish command
    
    	./docker/node8-dev exec npm run publish --[service-name]

	    # this command will execute all of these synchronously:
    	lerna clean --yes 
	    lerna boostrap --hoist,
	    lerna publish -f --skip-npm --skip-git
	    ./docker/node8-dev exec gulp ${directory name in _src_ directory}-build
    
	    # It will create new version of package and 
	    # put it to the private registry.
    
    **Example:**
    
    	# `--analytics` is based on directory name in `src` directory
	    ./docker/node8-dev exec npm run publish --analytics
    
    	./docker/node8-dev exec npm run publish --payment
	    ./docker/node8-dev exec npm run publish --publicapi
    	./docker/node8-dev exec npm run publish --pas
    
	    # etc.

## Question
For further information about lerna, please check its repo _https://github.com/lerna/lerna_.

For further information about dev code, please ask **win@payfazz.com**, **ricky@payfazz.com**, or **jefryanto@payfazz.com** or other developers.